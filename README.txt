Three scripts for monitoring network and CPU usage when it's not practical
to install a full-blown network monitoring system such as JFFNMS.

* snmpsar.sh - creates sar-like logs for remote hosts using SNMP v2 or v3
* routemon.sh - logs dynamic route nexthops using SNMP v2 or v3
* pingmon.sh - creates sar-like logs of packet loss and round-trip time
* netstatshot.sh - takes snapshots of the output of netstat -natp

Released as public domain with no warranty.

Michael Smith
msmith@cbnco.com
June 2007


snmpsar.sh
----------
Creates sar-like logs for remote hosts using SNMP v2 or v3. Requires
net-snmp on the client side. Tracks the following statistics:
* CPU (Linux, Cisco VPN Concentrator, Cisco)
* Network: packets per second and bytes per second on each interface
* Number of active IPsec tunnels (Cisco VPN Concentrator)

Usage:
1) Create a file containing SNMP parameters - see snmpcmd(1). There are
   examples for v2 and v3 in snmpconf.v2hosts and snmpconf.v3hosts,
   respectively.
2) Run from cron as often as you like:

   */5 * * * * snmpsar.sh -d /var/log/poller -s /etc/snmpconf.v3hosts \
	ip.or.hostname1 [ip.or.hostname2 [...]]

snmpsar creates files in
	/var/log/poller/snmpsar.YYYY-MM-DD.hostname.cpu
	/var/log/poller/snmpsar.YYYY-MM-DD.hostname.net
	/var/log/poller/snmpsar.YYYY-MM-DD.hostname.cisco_tunnelcount

Example CPU output:

                        cpu%
13:13:12/1172083692:      2
13:13:53/1172083733:      2
13:13:40/1172083780:      2

Example network output:

                        IFACE   rxpck/s txpck/s rxbyt/s txbyt/s rxerr/s txerr/s
13:13:28/1172084248     eth0    0.67    1.44    108.53  241.28  0.00    0.00
13:13:28/1172084248     eth1    0.00    0.00    0.00    0.00    0.00    0.00
13:13:28/1172084248     eth2    0.00    0.00    0.00    0.00    0.00    0.00
13:13:28/1172084248     eth3    0.00    0.00    0.00    0.00    0.00    0.00
13:13:28/1172084248     eth4    0.00    0.00    0.00    0.00    0.00    0.00
13:13:28/1172084248     lo      0.00    0.00    0.00    0.00    0.00    0.00

Example Cisco tunnel count output:

                        active  since last period
03:20:00/1149736200:     35       0
03:25:00/1149736500:     35       0
03:30:00/1149736800:     34      -1
03:35:00/1149737100:     35       2
03:40:00/1149737400:     35       1
03:45:00/1149737700:     35       0


routemon.sh
-----------
Logs dynamic route nexthops using SNMP v2 or v3. Requires net-snmp on the
client side, and an agent supporting the IP-FORWARD-MIB on the monitored
host.

For example, if you're using OSPF and have redundant routing, routemon can
tell you how much of the time you are using the primary and secondary
nexthops.

I am not sure stock net-snmp provides support for IP-FORWARD-MIB
in its agent, but I have had luck using Quagga/Zebra's OSPF SMUX agent.
In your snmpd.conf:

smuxsocket 127.0.0.1
smuxpeer .1.3.6.1.4.1.3317.1.2.5

And in ospfd.conf:

smux peer .1.3.6.1.4.1.3317.1.2.5

routemon usage:
1) Create a file containing SNMP parameters - see snmpcmd(1). There are
   examples for v2 and v3 in snmpconf.v2hosts and snmpconf.v3hosts,
   respectively.
2) Create a file containing a list of routers to query, along with their
   routes and expected nexthops.
3) Run from cron as often as you like:

   * * * * * routemon.sh -d /var/log/poller -r /etc/routemon.list \
	-s /etc/snmpconf.v3hosts

routemon creates files in /var/log/poller/<router name from the route list>.
Example:

2007-01-19/16:08:27/1169240907 0 route-not-found
2007-01-19/16:09:04/1169240944 1 172.22.7.2


pingmon.sh
----------

Usage: run from cron however often you like:
   * * * * * pingmon.sh /var/log/poller host1 [host2 [...]]

Example output:

                        HOST            loss%   rttavg

14:14:03/1172085723     172.16.20.2     0       0.141
14:14:03/1172085723     192.168.47.200  5       153.681

14:14:45/1172085765     172.16.20.2     0       0.139
14:14:45/1172085765     192.168.47.200  0       157.413


netstatshot.sh
--------------

Usage: run from cron:
   * * * * * netstatshot.sh /var/log/poller
