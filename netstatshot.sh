#!/bin/bash
# should run as root...

usage()
{
	echo "usage: $0 outdir" 1>&2
	exit 1
}

outdir=$1

[ -n "$outdir" ] || usage
if ! [ -d "$outdir" ]
then
	echo "outdir $outdir does not exist" 1>&2
	exit 1
fi

netstat -natp > "$outdir/netstat.$(date +%Y-%m-%d.%H.%M.%S)"
