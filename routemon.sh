#!/bin/bash
#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:
#
# Creates logfiles to track the nexthop for a list of dynamic routes.

usage()
{
    echo "usage: $0 -d logdir -r routelist -s snmpconffile" 1>&2
    exit 1
}

# routines for converting a prefix to a netmask
dooctet()
{
    local prefix=$1
    local bytenum=$2
    prefix=$((prefix - 8*bytenum))
    [ $prefix -ge 8 ] && echo 255 && return
    echo $((256 - 2**(8-$prefix)))
}

dooctets()
{
    local prefix=$1
    shift
    for i in $*
    do
        dooctet $prefix $i
    done | tr '\012' . | sed 's,.$,\n,'
}

prefix2octets()
{
    dooctets $1 0 1 2 3
}
prefix2octets_reversed()
{
    dooctets $1 3 2 1 0
}


routelist=
outdir=
snmpconf=

while getopts "d:r:s:" option
do
    case "$option" in
        d) outdir=$OPTARG ;;
        r) routelist=$OPTARG ;;
        s) snmpconf=$OPTARG ;;
        *) usage ;;
    esac
done

if [ -n "$routelist" ] && [ -n "$outdir" ] && [ -n "$snmpconf" ]
then
    :
else
    echo "Missing parameters." 1>&2
    usage
fi

if [ ! -r "$routelist" ]
then
    echo "Can't read route list '$routelist'." 1>&2
    exit 1
fi

snmpargs=$(cat "$snmpconf") || {
    echo "Can't read SNMP argument file '$snmpconf'." 1>&2
    exit 1
}

if [ ! -d "$outdir" ]
then
    echo "Output directory '$outdir' does not exist." 1>&2
    exit 1
fi

stamp="$(date +%Y-%m-%d/%H:%M:%S/%s)"

while read name router route nexthops
do
    case "$name" in
        ""|"#"*) continue ;;
    esac

    # 10.11.7.0/26 -> 10.11.7.0.192.255.255.255
    ip=${route%/*}
    prefix=${route#*/}
    revmask=$(prefix2octets_reversed "$prefix")
    target="$ip.$revmask"

    # -Ov: display value only, not OID name
    # -Oq: display value only, not type
    nexthop=$(snmpwalk -Ov -Oq $snmpargs "$router"    \
        "IP-FORWARD-MIB::ipCidrRouteNextHop.$target")

    # Now figure out which nexthop it's using.
    # 0 for none, 1 for first nexthop, 2 for second, -1 for unexpected nexthop
    if [ -z "$nexthop" ]
    then
        hopcode=-1
        hopdescription=snmp-failure
    elif echo "$nexthop" | grep -q -i "no such instance"
    then
        # We treat this as the route not existing, but it could also
        # mean the SNMP instance doesn't have IP-FORWARD-MIB.
        hopcode=0
        hopdescription=route-not-found
    else
        hopcode=-1
        hopdescription=$nexthop

        index=1
        for candidate in $nexthops
        do
            if [ "$candidate" = "$nexthop" ]
            then
                hopcode=$index
                break
            fi
            index=$((index + 1))
        done
    fi

    if ! echo "$stamp $hopcode $hopdescription" >> "$outdir/$name"
    then
        echo "Can't write to $outdir/$name." 1>&2
        exit 1
    fi
done < "$routelist"
