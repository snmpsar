#!/bin/bash
#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:
#
# Creates logfiles to track the nexthop for a list of dynamic routes.

usage()
{
    echo "usage: $0 -d logdir -s snmpconffile host1 [host2 [...]]" 1>&2
    exit 1
}

hosts=
outdir=
snmpconf=

while getopts "d:s:" option
do
    case "$option" in
        d) outdir=$OPTARG ;;
        s) snmpconf=$OPTARG ;;
        *) usage ;;
    esac
done

shift $((OPTIND-1))
hosts="$@"

if [ -n "$hosts" ] && [ -n "$outdir" ] && [ -n "$snmpconf" ]
then
    :
else
    echo "Missing parameters." 1>&2
    usage
fi

snmpargs=$(cat "$snmpconf") || {
    echo "Can't read SNMP argument file '$snmpconf'." 1>&2
    exit 1
}

if [ ! -d "$outdir" ]
then
    echo "Output directory '$outdir' does not exist." 1>&2
    exit 1
fi

filestamp="$(date +%Y-%m-%d)"
timestamp="$(date +%H:%M:%S/%s)"

get()
{
    local host=$1
    local oid=$2

    # -Ov: display value only, not OID name
    # -Oq: display value only, not type
    val=$(snmpget -Ov -Oq $snmpargs "$host" "$oid" 2>/dev/null) || return 1

    [ -n "$val" ] || return 1

    echo "$val" | grep -qi "No Such" && return 1

    echo "$val"
    return 0
}

logcpu()
{
    local file="$1"
    local timestamp="$2"
    local h="$3"

    # linux: 100 - UCD-SNMP-MIB::ssCpuIdle.0
    # cisco vpn cpu: alGeneralGaugeCpuUtil (1.3.6.1.4.1.3076.2.1.2.25.1.2.0)
    # cisco cpu: cpmCPUTotal5minRev (1.3.6.1.4.1.9.9.109.1.1.1.1.8)
    # or older version cpmCPUTotal5min (1.3.6.1.4.1.9.9.109.1.1.1.1.5)
    {
        idle=$(get "$h" "UCD-SNMP-MIB::ssCpuIdle.0") &&
            cpu=$((100 - idle))
    } ||
        cpu=$(get "$h" 1.3.6.1.4.1.3076.2.1.2.25.1.2.0) ||
        cpu=$(get "$h" .1.3.6.1.4.1.9.9.109.1.1.1.1.8) ||
        cpu=$(get "$h" .1.3.6.1.4.1.9.9.109.1.1.1.1.5)

    [ -n "$cpu" ] || return 1

    [ -f "$file" ] || echo -e "\t\t\tcpu%" > "$file"
    echo -e "$timestamp:\t$(printf "% 3d" "$cpu")" >> "$file"
}

# Converts a file full of counts to a file full of deltas.
makedeltas()
{
    local last="$1"
    local current="$2"

    # if both datasets exist and are not empty
    if [ -s "$last" ] && [ -s "$current" ]
    then
        # paste the common lines together using join, then subtract
        # corresponding fields
        join "$last" "$current" |
        awk '{
            printf "%s ", $1
            numfields = (NF-1)/2
            for (i = 0; i < numfields; ++i) {
                printf "%s ", ($(i+2+numfields) - $(i+2))
            }
            print ""
        }'
    fi
}

# Converts a file full of rates to a file full of deltas.
# Takes input from a pipe.
# Leaves the first field untouched (line label).
makerates()
{
    local last="$1"
    local current="$2"

    t1=$(date -r "$last" +%s 2>/dev/null) || return 1
    t2=$(date -r "$current" +%s 2>/dev/null) || return 1

    if ! [ "$t1" -lt "$t2" ]    # time running backwards
    then
        cat
    else
        awk -v tdelta="$((t2 - t1))" '{
            printf "%s", $1
            for (i = 2; i <= NF; ++i) {
                printf " %.2f", ($(i) / tdelta)
            }
            print ""
        }'
    fi
}

dorates()
{
    local last="$1"
    local current="$2"

    makedeltas "$last" "$current" |
        makerates "$last" "$current" |
        (echo ; sed -e "s,^,$timestamp\t," -e 's, ,\t,g') 

    mv "$file.current" "$file.last"
}


lognet()
{
    local file="$1"
    local timestamp="$2"
    local h="$3"

    tmp=$(mktemp "$file.XXXXXX")

    # Cisco VPN3030 needs -CB
    if snmptable $snmpargs -Cf '|' -CB "$h" IF-MIB::ifTable > "$tmp" &&
       [ "$(wc -l "$tmp" | awk '{print $1}')" -gt 3 ]
    then
        :
    else
        rm -f "$tmp"
        return 1
    fi

    [ -f "$file" ] || echo -e "\t\t\tIFACE\trxpck/s\ttxpck/s\trxbyt/s\ttxbyt/s\trxerr/s\ttxerr/s" > "$file"

    # SNMP table: IF-MIB::ifTable
    #
    # ifIndex|ifDescr|ifType|ifMtu|ifSpeed|ifPhysAddress|ifAdminStatus|ifOperStatus|ifLastChange|ifInOctets|ifInUcastPkts|ifInNUcastPkts|ifInDiscards|ifInErrors|ifInUnknownProtos|ifOutOctets|ifOutUcastPkts|ifOutNUcastPkts|ifOutDiscards|ifOutErrors|ifOutQLen|ifSpecific
    # 1|eth0|ethernetCsmacd|1500|100000000|0:40:63:e7:17:44|up|up|0:0:00:00.00|11969685|99653|0|0|0|0|34327404|375360|0|0|0|0|SNMPv2-SMI::zeroDotZero
    awk -F'|' '
        FNR == 1 || FNR == 2 { next }
        FNR == 3 {
            # build field name to column mapping
            split($0, a)
            for (field in a) {
                fields[a[field]] = field
            }
            next
        }
        {
            split($0, a)
            desc = a[fields["ifDescr"]]
            gsub("[^a-zA-Z0-9._-]", "", desc)       # sanitize
            desc = substr(desc, 0, 7)

            # Handle duplicate descriptions (e.g. cisco vpn3000)
            if (desc in ifdupe) {
                desc = substr(desc, 0, 4) "[" a[fields["ifIndex"]] "]"
            }
            ifdupe[desc]++

            print desc, \
                (a[fields["ifInUcastPkts"]] + a[fields["ifInNUcastPkts"]]),   \
                (a[fields["ifOutUcastPkts"]] + a[fields["ifOutNUcastPkts"]]), \
                a[fields["ifInOctets"]], a[fields["ifOutOctets"]], \
                a[fields["ifInErrors"]], a[fields["ifOutErrors"]]
        }
    ' "$tmp" | grep -v -e ^shaper | sort > "$file.current"

    dorates "$file.last" "$file.current" >> "$file"

    rm -f "$tmp"
}


log_cisco_tunnelcount()
{
    local file="$1"
    local timestamp="$2"
    local h="$3"

    active=$(get "$h" .1.3.6.1.4.1.9.9.171.1.2.1.1.0) || return 1
    prev=$(get "$h" .1.3.6.1.4.1.9.9.171.1.2.1.2.0) || return 1
    total=$((active + prev))

    totaldelta=0
    if [ -f "$file.lasttotal" ]
    then
        prev=$(cat "$file.lasttotal")
        totaldelta=$((total - prev))
    fi
    echo "$total" > "$file.lasttotal"

    [ -f "$file" ] || echo -e "\t\t\tactive\tsince last period" > "$file"
    echo -e "$timestamp:\t$(printf "% 3d\t% 3d" "$active" "$totaldelta")" \
        >> "$file"
}


for h in $hosts
do
    # If h=tcp:some.ip.addr, convert the ':' to a '.'
    fileh=$(echo "$h" | tr ':' '.' | tr -cd 'a-zA-Z0-9._-')
    basename="$outdir/snmpsar.$filestamp.$fileh"
    logcpu "$basename.cpu" "$timestamp" "$h"
    lognet "$basename.net" "$timestamp" "$h"

    log_cisco_tunnelcount "$basename.cisco_tunnelcount" "$timestamp" "$h"
done
