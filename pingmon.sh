#!/bin/bash
#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:
#
# Pings a list of hosts and saves the summary to the directory given in the
# first argument.

usage()
{
    echo "usage: $0 outdir host1 [host2 [...]]" 1>&2
    exit 1
}

outdir=$1
shift
hosts="$@"

if [ -n "$outdir" ] && [ -n "$hosts" ]
then
    :
else
    usage
fi

if ! [ -d "$outdir" ]
then
    echo "output directory $outdir does not exist" 1>&2
    exit 1
fi

filestamp="$(date +%Y-%m-%d)"
timestamp="$(date +%H:%M:%S/%s)"
file="$outdir/pingmon.$filestamp"

if ! [ -f "$file" ]
then
    echo -e "\t\t\tHOST\t\tloss%\trttavg" > "$file"
fi

for h in $hosts
do
    ping -q -c 50 -i0.5 "$h" | tail -2 > "$outdir/pending.$h" &
    sleep 0.1
done

wait

echo >> "$file"
for h in $hosts
do
    p="$outdir/pending.$h"

    # 20 packets transmitted, 20 received, 0% packet loss, time 9500ms
    # rtt min/avg/max/mdev = 0.132/0.143/0.157/0.013 ms

    pktloss=$(grep "packet loss" "$p" | sed -e 's,^.* \([0-9.]\+\)%.*$,\1,')

    rttavg=$(grep rtt "$p" |
             sed -e 's,^.*\([0-9.]\+\)/\([0-9.]\+\)/\([0-9.]\+\)/\([0-9.]\+\).*$,\2,')
    echo -e "$timestamp\t$h\t$pktloss\t$rttavg" >> "$file"

    rm -f "$p"
done
